import socket
#Facade class that does the parsing of the message that is going to be sent to the server
#in order to make it easier to the user
class Facade:
    #initialize client
    def __init__(self):
        self.cliente = Cliente()
        self.__objetos__ = []
    #add object to the set to be sent
    def __agregar_objetos__(self,objeto):
        self.__objetos__.append(objeto)
    #Crea objeto y lo agrega a la lista
    def crear_objeto(self,name,posX,posY,posZ,temp,color):
        obj = Objeto(name,posX,posY,posZ,temp,color)
        self.__agregar_objetos__(obj)
        print("Objeto creado y agregado a la lista")
    #Enviar la lista
    def enviar(self):
        buffer = ""
        temp = len(buffer)
        for obj in self.__objetos__:
            buffer += obj.aString()+"|"
        
        print (buffer[:temp - 1])
        self.cliente.conectar()
        self.cliente.enviar(buffer[:temp - 1])
    #Recibir mensaje
    def recibir(self):
        mes = self.cliente.recibir()
        self.cliente.cerrar()
        return mes
    def cerrar(self):
        self.cliente.cerrar()
#classs that creates an object and parses it in a string to be sent  
class Objeto:
    def __init__(self,name,posX,posY,posZ,temp,color):
        self.__name = name
        self.__posX = posX
        self.__posY = posY
        self.__posZ = posZ
        self.__temp = temp
        self.__color = color
    def aString(self):
        return (str(self.__name) + "," + str(self.__posX) + ","+ str(self.__posY) +","+ str(self.__posZ) +","+ str(self.__temp) + "," +str(self.__color))
#Class that is the client that communicates with the server
class Cliente:
    def __init__(self):
        self.client = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    def conectar(self):
        self.client.connect(('192.168.100.21', 12345))
    def enviar(self,mensaje):
        self.client.send(bytearray(mensaje, 'ascii'))
    def recibir(self):
        return self.client.recv(4096)
    def cerrar(self):
        self.client.close()
'''   
def main():
    #creamos una mesa, una silla y un televisor
    fac = Facade()
    #Mesa
    fac.crear_objeto("Mesa",0,0,3,32,"Cafe")
    #Silla
    fac.crear_objeto("Silla",0,0,1,32,"Azul")
    #Televisor
    fac.crear_objeto("TV",5,5,0,32,"Negro")
    
    #Envio de datos al servidor
    fac.enviar()
    fac.cerrar()
    #Recibo de datos del servidor
    #print(fac.recibir())

main()
'''

