#Tokken de todos los objetos (animales,personas y objetos)  
class Tokken(object):
    def __init__(self,idx,nombre,porcentaje,frame,posiciones):
        self.idx = idx
        self.nombre = nombre
        self.porcentaje = porcentaje
        self.frame = frame
        self.posiciones = posiciones
        
        
    #Getters and Setters
    def getNombre(self):
        return self.nombre
    def getIdx(self):
        return self.idx
    def getDistancia(self):
        return self.distancia
    def getVelocidad(self):
        return self.velocidad
    def getAlto (self):
        return self.alto
    def getAncho (self):
        return self.ancho
    def getColor (self):
        return self.color
    def getPorcentaje(self):
        return self.porcentaje
    def getFrame(self):
        return self.frame
    def getPosiciones(self):
        return self.posiciones
    def getColors(self):
        return self.COLORS


# Tokken para los sensores que no dependen del objeto, si no mas bien del ambiente
class Tokken_SensoresExtra(object):
    def __init__(self, temperatura, gas):
        self.temperatura = temperatura
        self.gas = gas
    def getTemperatura (self):
        return self.temperatura
    def getGas (self):
        return self.gas


#Caracteristica RGB
class Color(object):
    def __init__(self,Rojo,Verde,Azul):
        self.Rojo = Rojo
        self.Verde = Verde
        self.Azul =  Azul
    def getRojo(self):
        return self.Rojo
    def getVerde(self):
        return self.Verde
    def getAzul(self):
        return self.Azul



# Clase de los objetos entrantes que son reconocidos   
class Reconocido(object):
    def __init__(self):
        #Tienen la forma Nombre, ColorMayor, Distancia, Velocidad, Alto, Ancho
        self.Animales = [] 
        self.Objetos = []  
        self.Personas = []
        self.SensoresExtra =[] #Sensores de Temperatura y Gas
        self.frames = 0
    #Tokken
    def addAnimal(self,Animal):
        self.Animales.append(Animal)
    def addPersona(self,Persona):
        self.Personas.append(Persona)
    def addObjeto(self,Objeto):
        self.Objetos.append(Objeto)
    #Tokken_SensoresExtra
    def addSensoresExtra(self,Sensores):
        self.SensoresExtra.append(Sensores)

    def getAnimales(self):
        return self.Animales
    def getPersonas(self):
        return self.Personas
    def getObjetos(self):
        return self.Objetos
    def getFrames(self):
        return self.frames

    def setFrames(self,Frames):
        self.frames = Frames

