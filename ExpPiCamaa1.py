from picamera import PiCamera
from time import sleep

camera = PiCamera()


camera.start_preview()
sleep(4)
camera.capture('/home/pi/lookahead/lookimage2.jpg')
camera.start_recording('/home/pi/video.h264')
sleep(10)
camera.stop_recording()
camera.stop_preview()
