from predicciones3 import *
from Estructuras import *
#Obtiene la lista de todos los tokkens que existan en las imagenes
from ClientInterface import *

# Promedia la cantidad de veces y porcentajes que aparece una persona en un FRAME
# Falta añadir en que ángulo lo ubica      
def Promedio(Lista):
        if len(Lista) != 0:
                promedio = 0
                for i in Lista:
                       promedio += i.porcentaje
                return promedio / len(Lista)
        else:
                return 0
#Imprime todos los tokkens de una lista       
def Print(Lista):
    if len(Lista) != 0:
        for i in Lista:
            print(i.getNombre(),i.getPorcentaje(),i.getFrame())
#Obtiene el promedio de un solo tipo de IDX y genera un tupla (type, probabilidad)
def Prom_Type (Lista, Retorn, Type, Reconocidos):
    
    if Lista == []:
            return Retorn
    elif len(Lista) != 0:
        prom = 0
        Lista_Recur = []
        time = 0
        for i in Lista:
                if len(i) != 0:
                        prom += i[0].getPorcentaje()
                        if len(i[1:])!=0: 
                                Lista_Recur.append(i[1:])
                time += 1
        #print("Type: ", Type, ", ","time: ",time)
        #Porcentaje de aprobación minimo, al menos un 50% debe 
        if prom == 0 or int(0.1* Reconocidos.getFrames()) > time:
                
                return Prom_Type(Lista_Recur, Retorn,Type,Reconocidos)
        
        else:
               
                return Prom_Type(Lista_Recur, Retorn+[(Type, prom/time)],Type,Reconocidos)

#Obtiene todos los elementos en una lista, cuyo IDX sea igual
def getAll(Lista,ID):
        Aux = []
        for i in Lista:
                if i.idx == ID:
                        Aux.append(i)
        
        return Aux
#Obtiene todas las probabilidades de cada animal en la lista de animales
def getTAnimales(Lista,Reconocidos):
       
        if len(Lista) != 0:
                Frames = []
                One_Type = []
                Encontrado = []
                
                for i in ANIMALS:
                        
                        for fframe in Lista: #Un frame
                                L = getAll(fframe,i)
                                
                                if L != []:
                                        Frames.append(L)
                        if Frames != []:
                                K = Prom_Type(Frames,[],CLASSES[i],Reconocidos)
                                if K != []:
                                        Encontrado.append(K[0])
                                Frames = []
                        
                                
                return Encontrado
        else:
                
                return []
                                            
                                                
#Obtiene todas las probabilidades de cada objeto en la lista de objetos
def getTObjetos(Lista,Reconocidos):
        if len(Lista) != 0:
                Frames = []
                One_Type = []
                Encontrado = []
                
                for i in OBJETOS_:
                        
                        for fframe in Lista: #Un frame
                                L = getAll(fframe,i)
                                
                                if L != []:
                                        Frames.append(L)
                        if Frames != []:
                                K = Prom_Type(Frames,[],CLASSES[i],Reconocidos)
                                if K != []:
                                        Encontrado.append(K[0])
                                Frames = []
                        
                                
                return Encontrado
        else:
                
                return []

                
#Retorna todas las probabilidades de los tokkens   
def Objetos_Encontrados():
        Reconocidos = Analizar_Imagenes()
        Found = []
        Found.append(Prom_Type(Reconocidos.getPersonas(),[],'person',Reconocidos))
        Found.append(getTAnimales(Reconocidos.getAnimales(),Reconocidos))
        Oa = getTObjetos(Reconocidos.getObjetos(),Reconocidos)
        if Oa == []:
                Oa.append(('obstacle',100.0))
        Found.append(Oa)
        
        return Found


#Función principal
def __main__():
        
        
        print("Salida: ", Objetos_Encontrados())
        
         
#__main__()
