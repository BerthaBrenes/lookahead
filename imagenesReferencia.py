#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Apr 18 18:47:18 2019

@author: edgarchaves
"""

import os
import random
from glob import glob
from picamera.array import PiRGBArray
from picamera import PiCamera
import cv2
from imutils.video import VideoStream
from imutils.io import TempFile
import imutils
import numpy as np
import matplotlib.pyplot as plt
from keras import preprocessing
from time import sleep
import time

# initialize the camera and grab a reference to the raw camera capture
camera = PiCamera()
camera.resolution = (660, 620)
camera.framerate = 32
rawCapture = PiRGBArray(camera, size=(660, 620))
raw_frames_type_1 = []

# allow the camera to warmup
time.sleep(0.1)
lastTime = time.time()*1000.0

# get the reference to the webcam
#image = rawCapture.array

camera_height = 500
raw_frames_type_1 = []
raw_frames_type_2 = []
raw_frames_type_3 = []
raw_frames_type_4 = []

for frame in camera.capture_continuous(rawCapture, format="bgr", use_video_port=True):
	# grab the raw NumPy array representing the image, then initialize the timestamp
	# and occupied/unoccupied text
    image = frame.array
    gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    # read a new frame
  
   
   
    # add rectangle
    cv2.rectangle(image, (300, 75), (550, 325), (0, 255, 0), 2)

    # show the frame
    cv2.imshow("Capturing frames", image)

    key = cv2.waitKey(1) & 0xFF
    
    rawCapture.truncate(0)

    # quit camera if 'q' key is pressed
    if key & 0xFF == ord("q"):
        break
    elif key & 0xFF == ord("1"):
        # save the frame Edgar
        raw_frames_type_1.append(frame)
        camera.capture('./mainData/images_type_1/{}.png'.format(random.randrange(0, 1000, 1)))
        print('1 key pressed - saved TYPE_1 frame')
    elif key & 0xFF == ord("2"):
        # save the frame Celular
        raw_frames_type_2.append(frame)
        camera.capture('./mainData/images_type_2/{}.png'.format(random.randrange(1000, 2000, 1)))
        print('2 key pressed - Saved TYPE_2 frame')
    elif key & 0xFF == ord("3"):
        # save the frame Cuaderno
        raw_frames_type_3.append(frame)
        camera.capture('./mainData/images_type_3/{}.png'.format(random.randrange(2000, 3000, 1)))
        print('3 key pressed - Saved TYPE_3 frame')
    elif key & 0xFF == ord("4"):
        # save the frame Audifonos
        raw_frames_type_4.append(frame)
        camera.capture('./mainData/images_type_4/{}.png'.format(random.randrange(3000, 4000, 1)))
        print('4 key pressed - Saved TYPE_4 frame')


cv2.destroyAllWindows()

"""
save_width = 299
save_height = 299

for i in range(1, 5):
    name = './mainData/images_type_{}'.format(i)
    os.makedirs(name, exist_ok=True)


for i, frame in enumerate(raw_frames_type_1):
    roi = frame[75+2:425-2, 300+2:650-2]
    roi = cv2.cvtColor(roi, cv2.COLOR_BGR2RGB)
    roi = cv2.resize(roi, (save_width, save_height))
    cv2.imwrite('./mainData/images_type_1/{}.png'.format(i), cv2.cvtColor(roi,cv2.COLOR_BGR2RGB))

for i, frame in enumerate(raw_frames_type_2):
    roi = frame[75+2:425-2, 300+2:650-2]
    roi = cv2.cvtColor(roi, cv2.COLOR_BGR2RGB)
    roi = cv2.resize(roi, (save_width, save_height))
    cv2.imwrite('./mainData/images_type_2/{}.png'.format(i), cv2.cvtColor(roi,cv2.COLOR_BGR2RGB))

for i, frame in enumerate(raw_frames_type_3):
    roi = frame[75+2:425-2, 300+2:650-2]
    roi = cv2.cvtColor(roi, cv2.COLOR_BGR2RGB)
    roi = cv2.resize(roi, (save_width, save_height))
    cv2.imwrite('./mainData/images_type_3/{}.png'.format(i), cv2.cvtColor(roi,cv2.COLOR_BGR2RGB))

for i, frame in enumerate(raw_frames_type_4):
    roi = frame[75+2:425-2, 300+2:650-2]
    roi = cv2.cvtColor(roi, cv2.COLOR_BGR2RGB)
    roi = cv2.resize(roi, (save_width, save_height))
    cv2.imwrite('./mainData/images_type_4/{}.png'.format(i), cv2.cvtColor(roi,cv2.COLOR_BGR2RGB))

"""
