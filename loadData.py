#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Apr 18 19:25:05 2019
@author: edgarchaves
----------------------------------------"""
#Referencia principal
#https://www.youtube.com/watch?v=2Q4L3MtdAbY&list=LLAj9T6BeTgDe-78fdc00Lzg&index=4&t=0s


#130 y 130 funcionó muy bien.............
#width = 130
#height = 130


#140 y 140 funcionó mejor

width = 140
height = 140

import os
import random
from glob import glob
from keras import preprocessing


import cv2
import numpy as np
import matplotlib.pyplot as plt


from keras.utils import to_categorical

from keras.models import Sequential
from keras.layers.core import Activation, Dropout, Flatten, Dense
from keras.layers.convolutional import Convolution2D, MaxPooling2D
from keras.optimizers import Adam


class_names = ['EDGAR', 'CELULAR', 'CUADERNO', 'AUDIFONOS']


def load_images(base_path):
    images = []
    path = os.path.join(base_path, '*.png')
    for image_path in glob(path):
        image = preprocessing.image.load_img(image_path,
                                             target_size=(width, height))
        x = preprocessing.image.img_to_array(image)

        images.append(x)
    return images

images_type_1 = load_images('./mainData/images_type_1')
images_type_2 = load_images('./mainData/images_type_2')
images_type_3 = load_images('./mainData/images_type_3')
images_type_4 = load_images('./mainData/images_type_4')

plt.figure(figsize=(12,8))

#PARA EDGAR
for i in range(46):
    plt.subplot(1, 46, i+1)
    image = preprocessing.image.array_to_img(random.choice(images_type_1))
    plt.imshow(image)
    
    plt.axis('off')
    plt.title('{} image'.format(class_names[0]))

# show the plot
plt.show()


#PARA BOTELLA
for i in range(30):
    plt.subplot(1, 30, i+1)
    image = preprocessing.image.array_to_img(random.choice(images_type_2))
    
    plt.imshow(image)
    
    plt.axis('off')
    plt.title('{} image'.format(class_names[1]))
    

# show the plot
plt.show()

#PARA CORTAUNAS

for i in range(46):
    plt.subplot(1, 46, i+1)
    image = preprocessing.image.array_to_img(random.choice(images_type_3))
    plt.imshow(image)
    
    plt.axis('off')
    plt.title('{} image'.format(class_names[2]))

# show the plot
plt.show()

#PARA AUDIFONOS

for i in range(33):
    plt.subplot(1, 33, i+1)
    image = preprocessing.image.array_to_img(random.choice(images_type_4))
    plt.imshow(image)
    
    plt.axis('off')
    plt.title('{} image'.format(class_names[3]))

# show the plot
plt.show()


X_type_1 = np.array(images_type_1)
X_type_2 = np.array(images_type_2)
X_type_3 = np.array(images_type_3)
X_type_4 = np.array(images_type_4)


X = np.concatenate((X_type_1, X_type_2, X_type_3, X_type_4), axis=0)

X = X / 255.

X.shape

y_type_1 = [0 for item in enumerate(X_type_1)]
y_type_2 = [1 for item in enumerate(X_type_2)]
y_type_3 = [2 for item in enumerate(X_type_3)]
y_type_4 = [3 for item in enumerate(X_type_4)]

y = np.concatenate((y_type_1, y_type_2, y_type_3, y_type_4), axis=0)

y = to_categorical(y, num_classes=len(class_names))

print(y.shape)

"------------------------------------------------------------------------------"

#RED NEURONAL DE CONVOLUCIÓN
conv_1 = 16
conv_1_drop = 0.2
conv_2 = 32
conv_2_drop = 0.2
dense_1_n = 1024
dense_1_drop = 0.2
dense_2_n = 512
dense_2_drop = 0.2
lr = 0.001

epochs = 30
batch_size = 32
color_channels = 3

def build_model(conv_1_drop=conv_1_drop, conv_2_drop=conv_2_drop,
                dense_1_n=dense_1_n, dense_1_drop=dense_1_drop,
                dense_2_n=dense_2_n, dense_2_drop=dense_2_drop,
                lr=lr):
    model = Sequential()

    model.add(Convolution2D(conv_1, (3, 3),
                            input_shape=(width, height, color_channels),
                            activation='relu'))
    model.add(MaxPooling2D(pool_size=(2, 2)))
    model.add(Dropout(conv_1_drop))

    model.add(Convolution2D(conv_2, (3, 3), activation='relu'))
    model.add(MaxPooling2D(pool_size=(2, 2)))
    model.add(Dropout(conv_2_drop))
        
    model.add(Flatten())
        
    model.add(Dense(dense_1_n, activation='relu'))
    model.add(Dropout(dense_1_drop))

    model.add(Dense(dense_2_n, activation='relu'))
    model.add(Dropout(dense_2_drop))

    model.add(Dense(len(class_names), activation='softmax'))

    model.compile(loss='categorical_crossentropy',
                  optimizer=Adam(lr=lr),
                  metrics=['accuracy'])

    return model


np.random.seed(1)
model = build_model()
model.summary()

epochs = 7

model.fit(X, y, epochs=epochs)

model.save('reconocedorObjetos.h5')
