#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri May 31 20:17:25 2019

@author: edgarchaves
"""

# import the necessary packages
import numpy as np
import argparse
import cv2
import os
import time
from Estructuras import *

#Clase con los argumentos para la IA
class Args:
	image = r''
	prototxt = r'C:\Users\brian\Desktop\MachineLearning\MobileNetSSD_deploy.prototxt.txt'
	model = r'C:\Users\brian\Desktop\MachineLearning\MobileNetSSD_deploy.caffemodel'
	confidence = 0.2      #Si es menor a esa coincidencia entonces no lo toma en cuenta

#Objetos que puede reconocer la inteligencia artificial
CLASSES = ["background", "aeroplane", "bicycle", "bird", "boat",
                "bottle", "bus", "car", "cat", "chair", "cow", "table",
                "dog", "horse", "motorbike", "person", "pottedplant", "sheep",
                "sofa", "train", "tvmonitor"]
#Id's que corresponden a animales en CLASSES
ANIMALS = [3,8,10,12,13,17]
OBJETOS_ = [0,1,2,4,5,6,7,9,11,14,16,18,19,20]
#Ruta de la carpeta de imagenes
carpeta = r'C:\Users\brian\Desktop\Imagenes'

#Retorna un objeto que contiene 3 listas con tokkens, según sea animal, persona u objeto.
def Analizar_Imagenes ():
        Reconocidos = Reconocido()
        frame = 0
        for archivo in os.listdir(carpeta):
                
                Args.image = os.path.join(carpeta,archivo)
                lab = Args.image
                
                args = vars(Args)

                # initialize the list of class labels MobileNet SSD was trained to
                # detect, then generate a set of bounding box colors for each class
                
                COLORS = np.random.uniform(0, 255, size=(len(CLASSES), 3))
                
                # load our serialized model from disk
                #print("[INFO] loading model...")
                net = cv2.dnn.readNetFromCaffe(args["prototxt"], args["model"])


                # load the input image and construct an input blob for the image
                # by resizing to a fixed 300x300 pixels and then normalizing it
                # (note: normalization is done via the authors of the MobileNet SSD
                # implementation)

                image = cv2.imread(args["image"])
                #image = cv2.imread("/Users/edgarchaves/Documents/data1/2.jpg")
                (h, w) = image.shape[:2]

                blob = cv2.dnn.blobFromImage(cv2.resize(image, (300, 300)), 0.007843,(300, 300), 127.5)

                # pass the blob through the network and obtain the detections and
                # predictions
                #print("[INFO] computing object detections...")
                net.setInput(blob)
                detections = net.forward()
                idx =0
                Lista_L = []
                # loop over the detections
                for i in np.arange(0, detections.shape[2]):
                        # extract the confidence (i.e., probability) associated with the
                        # prediction
                        confidence = detections[0, 0, i, 2]
                 
                        # filter out weak detections by ensuring the `confidence` is
                        # greater than the minimum confidence
                        if confidence > args["confidence"]:
                                # extract the index of the class label from the `detections`,
                                # then compute the (x, y)-coordinates of the bounding box for
                                # the object
                                idx = int(detections[0, 0, i, 1])
                                box = detections[0, 0, i, 3:7] * np.array([w, h, w, h])
                                (startX, startY, endX, endY) = box.astype("int")
                                positions = (startX, startY, endX, endY)
                                # display the prediction
                                label = "{}: {:.2f}%".format(CLASSES[idx], confidence * 100)
                                print("[INFO] {}".format(label),"Frame {}" .format(frame))
                                
                                #Confidence, Label
                                #cv2.rectangle(image, (startX, startY), (endX, endY),
                                 #       COLORS[idx], 2)
                                #y = startY - 15 if startY - 15 > 15 else startY + 15
                                #cv2.putText(image, label, (startX, y),
                                #        cv2.FONT_HERSHEY_SIMPLEX, 0.5, COLORS[idx], 2)
                                Lista_L.append(Tokken(idx, CLASSES[idx],confidence * 100,frame,positions))

                # show the output image
                #cv2.imshow("Output", image)

                cv2.destroyAllWindows()
                cv2.waitKey(0)
                if idx == 15:
                        Reconocidos.addPersona(Lista_L)
                elif idx in ANIMALS:
                        Reconocidos.addAnimal(Lista_L)
                else:
                        Reconocidos.addObjeto(Lista_L)
                frame+=1
        Reconocidos.setFrames(frame)
        return Reconocidos

        
#Función principal        

        


                

